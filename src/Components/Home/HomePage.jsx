import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import {
  getMakers,
  modalMenuHandle,
  makerDelete,
} from "../../redux/CarMaker/cm-action";
import "./HomePage.css";
import NewCompany from "./NewCompany/NewCompany";
class HomePage extends Component {
  componentDidMount() {
    this.props.getMakers();
  }
  render() {
    const { makers, modalMenu, modalMenuHandle, makerDelete } = this.props;
    return (
      <div className="main-section">
        <button onClick={modalMenuHandle} className="add-maker">
          Add
        </button>
        <div className="maker-section">
          {makers.map((maker) => {
            let style = {
              backgroundImage: `url(${maker.logo})`,
            };
            return (
              <div key={maker.id} className="maker-wrapper">
                <div style={style} className="maker"></div>
                <div className="maker-footer">
                  <Link
                    to={{
                      pathname: `/${maker.id}`,
                      state: [style, maker.name],
                    }}
                    className="maker-name"
                  >
                    {maker.name.toUpperCase()}
                  </Link>
                  <i
                    onClick={() => makerDelete(maker.id)}
                    className="fa fa-trash"
                    aria-hidden="true"
                  ></i>
                </div>
              </div>
            );
          })}
          {modalMenu && <NewCompany />}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    makers: state.makerReducer.makers,
    modalMenu: state.makerReducer.newCompanyModal,
  };
};

export default connect(mapStateToProps, {
  makerDelete,
  getMakers,
  modalMenuHandle,
})(HomePage);
