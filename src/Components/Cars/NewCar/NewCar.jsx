import React from "react";
import { connect } from "react-redux";
import {
  carsModalMenuHandle,
  newCarTitle,
  newCarPrice,
  newCarFuel,
  newCarTransmission,
  newCarMileAge,
  postNewCar,
} from "../../../redux/Cars/cars-action";

import "./NewCar.css";
function NewCar({
  newCarMileAge,
  newCarTransmission,
  newCarFuel,
  newCarPrice,
  carsModalMenuHandle,
  newCarTitle,
  postNewCar,
  makerId,
  newCar,
}) {
  return (
    <div className="new-car-overlay">
      <div className="new-car-main">
        <form className="new-car-form" action="">
          <div className="input-group">
            <label className="input-names">Name</label>
            <input
              onChange={(event) => {
                newCarTitle(event.target.value);
              }}
              className="new-car-input"
              type="text"
            />
          </div>
          <div className="input-group">
            <label className="input-names">Price</label>
            <input
              onChange={(event) => {
                newCarPrice(event.target.value);
              }}
              className="new-car-input"
              type="text"
            />
          </div>
          <div className="input-group">
            <label className="input-names">Fuel type</label>
            <input
              onChange={(event) => {
                newCarFuel(event.target.value);
              }}
              className="new-car-input"
              type="text"
            />
          </div>
          <div className="input-group">
            <label className="input-names">Transmission</label>
            <input
              onChange={(event) => {
                newCarTransmission(event.target.value);
              }}
              className="new-car-input"
              type="text"
            />
          </div>
          <div className="input-group">
            <label className="input-names">Mileage</label>
            <input
              onChange={(event) => {
                newCarMileAge(event.target.value);
              }}
              className="new-car-input"
              type="text"
            />
          </div>
        </form>
        <div className="new-car-btn">
          <button
            onClick={(event) => {
              postNewCar(newCar, makerId);
            }}
            className="new-car-add-btn"
          >
            Add
          </button>
          <button onClick={carsModalMenuHandle} className="new-car-cancel-btn">
            cancel
          </button>
        </div>
      </div>
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    newCar: state.carReducer.newCar,
  };
};

export default connect(mapStateToProps, {
  newCarFuel,
  newCarPrice,
  carsModalMenuHandle,
  newCarTitle,
  newCarTransmission,
  newCarMileAge,
  postNewCar,
})(NewCar);
