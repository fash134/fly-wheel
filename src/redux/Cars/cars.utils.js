export const fetchCars = async (id) => {
  let data = await fetch(
    `https://cardetailsapi.herokuapp.com/api/carmakers/cars/${id}`
  ).then((response) => response.json());
  return data;
};

export const postCars = async (newCarDetails, makerId, carId = 0) => {
  let data = await fetch(
    `https://cardetailsapi.herokuapp.com/api/carmakers/cars/${makerId}`,
    {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(newCarDetails),
    }
  ).then((response) => response.json());
  return data;
};

export const deleteCar = async (id) => {
  let data = await fetch(
    `https://cardetailsapi.herokuapp.com/api/carmakers/cars/${id}`,
    {
      method: "DELETE",
    }
  ).then((response) => response.json());
  return parseInt(data.id);
};

export const updateCar = async (newCarDetails, id) => {
  let data = await fetch(
    `https://cardetailsapi.herokuapp.com/api/carmakers/cars/${id}`,
    {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(newCarDetails),
    }
  ).then((response) => response.json());
  return data;
};
