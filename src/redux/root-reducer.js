import { combineReducers } from "redux";
import makerReducer from "./CarMaker/cm-reducer";
import carReducer from "./Cars/cars-reducer";

export default combineReducers({
  makerReducer,
  carReducer,
});
