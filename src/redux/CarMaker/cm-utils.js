export const fetchMaker = async () => {
  let data = await fetch(
    `https://cardetailsapi.herokuapp.com/api/carmakers`
  ).then((response) => response.json());
  return data;
};

export const postCompany = async (makerName) => {
  let newCompany = {
    name: makerName,
  };
  let data = await fetch(`https://cardetailsapi.herokuapp.com/api/carmakers`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(newCompany),
  }).then((response) => response.json());
  return data;
};

export const deleteMaker = async (id) => {
  let data = await fetch(
    `https://cardetailsapi.herokuapp.com/api/carmakers/${id}`,
    {
      method: "DELETE",
    }
  ).then((response) => response.json());
  return parseInt(data.id);
};
